# Preseed Creator Docker

Небольшой скрипт, предназначенный для облегчения создания собственого установочного ISO-образа Debian/Ubuntu.

Основано на проекте [Luc Didry](https://framagit.org/fiat-tux/hat-softwares/preseed-creator)

Скрипт адаптирован для сборки в Docker окружении, так как не использует монтирование через петлевые устройства iso образа

Для сборки необходимы следующие зависимости:


```
apt install wget genisoimage xorriso isolinux rsync cpio
```
